<?php



class Migrate {
    private $sourceDb;//源数据库
    protected $sourceConfig ; //源目标配置
    private $targetDb;//目标数据库

    public function __construct($sourceConfig,$targetConfig){
        $this->sourceDb = (new DataBaseModel())->connect($sourceConfig);
        $this->sourceConfig = $sourceConfig;
        $this->targetDb = (new DataBaseModel)->connect($targetConfig);
    }

    //获取源目标所有INNODB的数据并保存起来供迁移好后使用
    public function innoDbToMyIsam(){
        $innodbTables = $this->sourceDb->getInnoDbTablesName();
       //可在此进行存储
       $md5StorageFile = $this->storageFile();
       
       file_put_contents($md5StorageFile,implode(',',$innodbTables));
       //然后转成myisam
       if(!empty($innodbTables)){
        foreach($innodbTables as $table){
           $result = $this->sourceDb->toMyIsam($table);
           if($result !== true){
            echo $result."<br/>";
           }
        }
        echo '转换完毕';
       } 
    }

    public function myisamToInnoDb(){
        $file = $this->storageFile();
        if(!file_exists($file)){
            echo '没有存在转换的文件';die;
        }
        $tables = file_get_contents($file);
        $tables = explode(',',$tables);
        foreach($tables as $table){
            $result = $this->targetDb->toInnoDb($table);
            if($result !== true){
             echo $result."<br/>";
            }
        }
            
        
            
    }
    //迁移或者存储文件的路径
    public function storageFile(){
        return md5(implode(',',$this->sourceConfig)) . 'txt';
    }
}

class DataBaseModel{
    protected $conn;
    public $dbname;
    //数据库链接
    public  function connect($config){
        extract($config);
        $conn=mysqli_connect($host,$username,$password);
        if(!$conn) die("数据库系统连接失败！". mysqli_connect_error());
        $this->conn  = $conn;

        if(isset($config['dbname'])){
            $this->selectDb($config['dbname']);
            $this->dbname = $config['dbname'];
        }
        return $this;
    }

    /**
     * 获取所有数据库
     * return array
     */
    public  function getDatabasesName(){
        $showTables = mysqli_query($this->conn,"SHOW TABLES");	
        $result = array();
        while($row = mysqli_fetch_array($showTables)) {			
          $result[] = array_shift($row);	
        }
        return $result;
    }

    //设置数据库
    public function selectDb($dbname){
        mysqli_select_db($this->conn,$dbname);
    }

    /**
     * 获取所有Innodb的表名字
     */
    public function getInnoDbTablesName(){
        $sql = "SHOW TABLE STATUS FROM ".$this->dbname ."  where ENGINE='InnoDB'";
        $showTables = mysqli_query($this->conn,$sql);
        $result = array();
        while($row = mysqli_fetch_array($showTables)) {			
          $result[] = $row['Name'];	
        }
        return $result;
    }
    /**
     * 转换成MYISAM
     */
    public function toMyIsam($tablename){
        //ALTER TABLE TABLENAME ENGINE=MyISAM;
        $sql = sprintf('ALTER TABLE %s ENGINE=MyISAM;',$tablename);
        $query = mysqli_query($this->conn,$sql);
        if(!$query){
            return mysqli_error($this->conn);
        }
        return true;
    }
    /**
     * 转换成INNODB
     */
     public function toInnoDb($tablename){
        $sql = sprintf('ALTER TABLE %s ENGINE=InnoDB;',$tablename);
        $query = mysqli_query($this->conn,$sql);
        if(!$query){
            return mysqli_error($this->conn);
        }
        return true;
     }

}

