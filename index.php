<?php
/*
作者：xgh
邮箱：2131527683@qq.com
数据库物理迁移功能-单库操作，此功能可以实现多库，请自行扩展
原理：将源数据库data文件进行COPY到目标的数据库中
使用该方法需要进行对数据库进行做INNODB转换成MYISAM,不然会报错
当迁移好之后又使用方法进行对上面的MYISAM文件转换成INNODB
此程序就是做批量转换操作 
*/
include('Migrate.php');
include('config.php');
//1.配置源文件和目标文件的数据库

//2.将单库的表全部由INNODB转成MyISAM
$mObj = new Migrate($sourceConfig,$targetConfig);
$mObj->innoDbToMyIsam();

//3.将单表的data的erptest数据库进行打包
//4.然后将打包的内容上传到目标数据库解压。
//5.请执行recoverInnoDb.php

